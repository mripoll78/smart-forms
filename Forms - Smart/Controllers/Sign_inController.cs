﻿using Forms___Smart.Models.ModelsWeb;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace Forms___Smart.Controllers
{
    public class Sign_inController : Controller
    {
        public ActionResult Index()
        {
            string _token = Convert.ToString(Session["token"]);           

            if (_token != "")
            {   
                var handler = new JwtSecurityTokenHandler();
                var token = handler.ReadJwtToken(_token);
                if (token != null)
                {
                    DateTime fecha = DateTime.Now;
                    var foreachs = token.Claims;
                    foreach (var item in foreachs)
                    {
                        if (item.Type == "exp")
                        {
                            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                            dtDateTime = dtDateTime.AddSeconds(Convert.ToDouble(item.Value)).ToLocalTime();
                            fecha = dtDateTime;
                        }
                    }

                    if (DateTime.Now < fecha)
                    {
                        return RedirectToAction("index", "Home");
                    }
                    else
                    {
                        Session["token"] = null;
                        Session["id_users"] = null;

                        return View();
                    }
                }
                else
                {
                    Session["token"] = null;
                    Session["id_users"] = null;

                    return View();
                }
            }
            else
            {
                Session["token"] = null;
                Session["id_users"] = null;
                return View();
            }
        }

        [HttpPost]
        public ActionResult ValidarCredenciales(LoginRequestViewModels userlogin, FormCollection form)
        {            
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_SERVER"].ToString() + "LOGIN/AUTHENTICATE");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json =   "{\"username\":\"" + userlogin.username + "\"," + "\"password\":\"" + userlogin.password + "\"}";
                streamWriter.Write(json);
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                httpResponse.Close();
                streamReader.Close();

                if (result != null)
                {
                    LoginResultViewModels listProductos = JsonConvert.DeserializeObject<LoginResultViewModels>(result);
                    if (listProductos.status == "Error" || listProductos.Tipo == "Error")
                    {
                        userlogin.message = listProductos.Descripcion;
                        return View("Index", userlogin);
                    }
                    else
                    {
                        Session["id_users"] = listProductos.id_users;
                        Session["id_perfil"] = listProductos.id_perfil;
                        Session["id_status"] = listProductos.id_status;
                        Session["nombre"] = listProductos.nombre;
                        Session["username"] = listProductos.username;
                        Session["token"] = listProductos.token;

                        return RedirectToAction("index", "Home");
                    }                    
                }
                else
                {
                    userlogin.message = "Contraseña incorrecta";
                    return View("Index", userlogin);
                }
            }
        }
    }
}