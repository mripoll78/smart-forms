﻿using Forms___Smart.Models.ModelsWeb;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Forms___Smart.Controllers
{
    public class PersonasController : Controller
    {
        // GET: Personas
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Crear_personas()
        {
            object retuly = Session["token"];
            string API_KEY = Convert.ToString(retuly);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["URL_SERVER"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", API_KEY);

            var result = client.GetAsync("PEOPLE/GET_DATA_PEOPLE").Result.Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                GetDataPeopleViewModels listProductos = JsonConvert.DeserializeObject<GetDataPeopleViewModels>(result);
                if (listProductos != null && listProductos.Type == "Success")
                {
                    return View(listProductos);
                }
                else
                {
                    return RedirectToAction("index", "Error404");
                }
            }
            else
            {
                return RedirectToAction("index", "Error404");
            }
        }

        [HttpPost]
        public ActionResult Crear_personas(ObjCrearPersonas data)
        {
            object retuly = Session["token"];
            string API_KEY = Convert.ToString(retuly);

            IEnumerable<KeyValuePair<string, string>> Parametros = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("id_type_id",Convert.ToString(data.id_type_id)),
                new KeyValuePair<string, string>("identifications",Convert.ToString(data.identifications)),
                new KeyValuePair<string, string>("id_city",Convert.ToString(data.id_city)),
                new KeyValuePair<string, string>("id_gender",Convert.ToString(data.id_gender)),
                new KeyValuePair<string, string>("id_profile",Convert.ToString(data.id_profile)),
                new KeyValuePair<string, string>("names",Convert.ToString(data.names)),
                new KeyValuePair<string, string>("surnames",Convert.ToString(data.surnames)),
                new KeyValuePair<string, string>("password",Convert.ToString(data.password)),
                new KeyValuePair<string, string>("confirPassword",Convert.ToString(data.confirPassword)),
                new KeyValuePair<string, string>("usuario",Convert.ToString(data.usuario)),
                new KeyValuePair<string, string>("email",Convert.ToString(data.email)),
                new KeyValuePair<string, string>("phone",Convert.ToString(data.phone))
            };
            HttpContent Objet = new FormUrlEncodedContent(Parametros);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["URL_SERVER"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", API_KEY);

            var result = client.PostAsync("PEOPLE/POST_CREATE_PEOPLE", Objet).Result.Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                GetTypeResultViewModels listProductos = JsonConvert.DeserializeObject<GetTypeResultViewModels>(result);
                if (listProductos != null && listProductos.Type == "Success")
                {
                    return Json(listProductos);
                }
                else
                {
                    return Json(listProductos);
                }
            }
            else
            {
                return RedirectToAction("index", "Error404");
            }
        }

        public ActionResult Editar_personas(int id)
        {
            return View();
        }
    }
}