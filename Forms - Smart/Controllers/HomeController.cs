﻿using System.Web.Mvc;

namespace Forms___Smart.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
             return View();
        }
    }
}
