﻿using System.Web;
using System.Web.Optimization;

namespace Forms___Smart
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Content/js/vendor/jquery-1.12.4.min.js",
                          "~/Content/js/bootstrap.min.js",
                          "~/Content/js/wow.min.js",
                          "~/Content/js/jquery-price-slider.js",
                          "~/Content/js/jquery.meanmenu.js",
                          "~/Content/js/owl.carousel.min.js",
                          "~/Content/js/jquery.sticky.js",
                          "~/Content/js/jquery.scrollUp.min.js",
                          "~/Content/js/counterup/jquery.counterup.min.js",
                          "~/Content/js/counterup/waypoints.min.js",
                          "~/Content/js/counterup/counterup-active.js",
                          "~/Content/js/scrollbar/jquery.mCustomScrollbar.concat.min.js",
                          "~/Content/js/scrollbar/mCustomScrollbar-active.js",
                          "~/Content/js/metisMenu/metisMenu.min.js",
                          "~/Content/js/metisMenu/metisMenu-active.js",
                          "~/Content/js/morrisjs/raphael-min.js",
                          "~/Content/js/sparkline/jquery.sparkline.min.js",
                          "~/Content/js/sparkline/jquery.charts-sparkline.js",
                          "~/Content/js/sparkline/sparkline-active.js",
                          "~/Content/js/calendar/moment.min.js",
                          "~/Content/js/calendar/fullcalendar.min.js",
                          "~/Content/js/calendar/fullcalendar-active.js",
                          "~/Content/js/plugins.js",
                          "~/Content/js/main.js",
                          "~/Content/js/data-table/bootstrap-table.js",
                          "~/Content/js/data-table/tableExport.js" ,
                          "~/Content/js/data-table/data-table-active.js" ,
                          "~/Content/js/data-table/bootstrap-table-editable.js",
                          "~/Content/js/data-table/bootstrap-editable.js" ,
                          "~/Content/js/data-table/bootstrap-table-resizable.js",
                          "~/Content/js/data-table/colResizable-1.5.source.js",
                          "~/Content/js/data-table/bootstrap-table-export.js",
                          "~/Content/js/editable/jquery.mockjax.js",
                          "~/Content/js/editable/mock-active.js",
                          "~/Content/js/editable/select2.js",
                          "~/Content/js/editable/moment.min.js",
                          "~/Content/js/editable/bootstrap-datetimepicker.js",
                          "~/Content/js/editable/bootstrap-editable.js",
                          "~/Content/js/editable/xediable-active.js",
                          "~/Content/js/chart/jquery.peity.min.js",
                          "~/Content/js/peity/peity-active.js",
                          "~/Content/js/tab.js",
                          "~/Content/js/datapicker/bootstrap-datepicker.js",
                          "~/Content/js/datapicker/datepicker-active.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/css/bootstrap.min.css",
                        "~/Content/css/font-awesome.min.css",
                        "~/Content/css/owl.carousel.css",
                        "~/Content/css/owl.theme.css",
                        "~/Content/css/owl.transitions.css",
                        "~/Content/css/animate.css",
                        "~/Content/css/normalize.css",
                        "~/Content/css/meanmenu.min.css",
                        "~/Content/css/main.css",
                        "~/Content/css/educate-custon-icon.css",
                        "~/Content/css/morrisjs/morris.css",
                        "~/Content/css/scrollbar/jquery.mCustomScrollbar.min.css",
                        "~/Content/css/metisMenu/metisMenu.min.css",
                        "~/Content/css/metisMenu/metisMenu-vertical.css",
                        "~/Content/css/calendar/fullcalendar.min.css",
                        "~/Content/css/calendar/fullcalendar.print.min.css",
                        "~/Content/style.css",
                        "~/Content/css/datapicker/datepicker3.css",
                        "~/Content/css/responsive.css",
                        "~/Content/css/data-table/bootstrap-table.css",
                        "~/Content/css/data-table/bootstrap-editable.css"));
        }
    }
}
