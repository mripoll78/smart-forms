﻿using Forms___Smart.Models.ModelsWeb;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace Forms___Smart.Models.ClasesPrueba
{
    public class MetodoGet : Controller
    {
        [HttpGet]
        public ActionResult index()
        {
             return View();
        }

        [HttpGet]             
        public ActionResult Get()
        {
            object retuly = Session["token"];
            string API_KEY = Convert.ToString(retuly);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["URL_SERVER"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", API_KEY);

            var result = client.GetAsync("USERS/GET_USERS").Result.Content.ReadAsStringAsync().Result;
            if (result != null)
            {
                dynamic listProductos = JsonConvert.DeserializeObject(result);
                if (listProductos != null && listProductos.status == "Success")
                {
                    Session["id_users"] = listProductos.id_users;
                    Session["id_perfil"] = listProductos.id_perfil;
                    Session["id_status"] = listProductos.id_status;
                    Session["nombre"] = listProductos.nombre;
                    Session["username"] = listProductos.username;

                    return View();
                }
                else
                {
                    return RedirectToAction("index", "Error404");
                }
            }
            else
            {
                return RedirectToAction("index", "Error404");
            }
        }

        [HttpPost]
        public ActionResult Post(LoginRequestViewModels userlogin, FormCollection form)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_SERVER"].ToString() + "LOGIN/AUTHENTICATE");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"username\":\"" + userlogin.username + "\"," +
                                "\"password\":\"" + userlogin.password + "\"}";
                                //"\"descripcion\":\"" + descripcion + "\"," +
                                //"\"id_estado\":" + id_estado + "," +
                                //"\"id_usuario\":" + id_usuario + "," +
                                //"\"ex\":\" " + ex + "\"," +
                                //"\"url\":\" " + url + "\"," +
                                //"\"estado\":\" " + estado + "\"," +
                                //"\"id_general\":" + id_general + "," +
                                //"\"Payload\":\" " + payload + "\"}";

                streamWriter.Write(json);
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                httpResponse.Close();
                streamReader.Close();

                if (result != null)
                {
                    LoginResultViewModels listProductos = JsonConvert.DeserializeObject<LoginResultViewModels>(result);
                    if (listProductos.status == "Error")
                    {
                        userlogin.message = listProductos.Descripcion;
                        return View("Index", userlogin);
                    }
                    else
                    {
                        Session["id_users"] = listProductos.id_users;
                        Session["id_perfil"] = listProductos.id_perfil;
                        Session["id_status"] = listProductos.id_status;
                        Session["nombre"] = listProductos.nombre;
                        Session["username"] = listProductos.username;
                        Session["token"] = listProductos.token;

                        return RedirectToAction("index", "Home");
                    }
                }
                else
                {
                    userlogin.message = "Contraseña incorrecta";
                    return View("Index", userlogin);
                }
            }
        }
    }
}