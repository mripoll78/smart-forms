﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forms___Smart.Models.ModelsWeb
{
    public class LoginRequestViewModels
    {
        public string username { get; set; }
        public string password { get; set; }
        public int perfil { get; set; }
        public string message { get; set; }
        public Exception message_ex { get; set; }
    }
}