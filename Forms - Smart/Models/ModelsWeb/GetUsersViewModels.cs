﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forms___Smart.Models.ModelsWeb
{
    public class GetUsersViewModels
    {
        public int? id_users { get; set; }
        public int? id_perfil { get; set; }
        public int? id_status { get; set; }
        public string status { get; set; }
        public string nombre { get; set; }
        public string username { get; set; }
    }
}