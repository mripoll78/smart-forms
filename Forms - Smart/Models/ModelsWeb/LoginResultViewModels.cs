﻿using Forms___Smart.Models.View;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Forms___Smart.Models.ModelsWeb
{
    public class LoginResultViewModels
    {
        public int? id_users { get; set; }
        public int? id_perfil { get; set; }
        public int? id_status { get; set; }
        public string status { get; set; }
        public string nombre { get; set; }
        public string username { get; set; }
        public string token { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public string id { get; set; }

        [NotMapped]
        public List<Profile> ListProfile { get; set; }

        [NotMapped]
        public List<Gender> ListGender { get; set; }

        [NotMapped]
        public List<City> ListCity { get; set; }
    }
}