﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forms___Smart.Models.ModelsWeb
{
    public class ObjCrearPersonas
    {
        public Nullable<int> id_type_id { get; set; }
        public Nullable<int> id_profile { get; set; }
        public Nullable<int> id_city { get; set; }
        public Nullable<int> id_gender { get; set; }
        public string identifications { get; set; }
        public string names { get; set; }
        public string surnames { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }
        public string confirPassword { get; set; }
    }
}