﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Forms___Smart.Models.ModelsWeb
{
    public class GetDataPeopleViewModels
    {
        public int id { get; set; }
        public Nullable<int> id_type_id { get; set; }
        public Nullable<int> id_profile { get; set; }
        public Nullable<int> id_city { get; set; }
        public Nullable<int> id_gender { get; set; }
        public Nullable<int> identifications { get; set; }
        public string names { get; set; }
        public string surnames { get; set; }
        public string email { get; set; }        
        public string phone { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }
        public string confirPassword { get; set; }
        public List<ProfileViewModels> ListProfile { get; set; }
        public List<TipoIdViewModels> ListTipoId { get; set; }
        public List<GenderViewModels> ListGender { get; set; }
        public List<CityViewModels> ListCity { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
    }
}